class CfgPatches
{
	class DFS_stanceIndicator
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"Extended_Eventhandlers"};
	};
};

class Extended_PostInit_EventHandlers
{
	DFS_stanceIndicator_init = "call compile preprocessFileLineNumbers 'dfs\dfs_stanceindicator\scripts\init.sqf';";
};

class RscTitles
{
	class DFS_RscStanceIndicator
	{
		idd = 460829;
		movingEnable = "false";
		duration = 1000000;
		fadein = 0;
		name = "DFS_RscStanceIndicator";
		onLoad = "with uiNameSpace do { DFS_RscStanceIndicator_idd = _this select 0; };";
		
		class Controls
		{
			class StanceSiluette
			{
				idc = 4601;
				type = 0;
				style = 0x30 + 0x800;
				colorText[] = {1, 1, 1, 1};
				colorBackground[] = {0, 0, 0, 0};
				font = "Zeppelin32";
				sizeEx = 0.032;
				x = "safeZoneX+safeZoneW-0.13";
				y = "safeZoneY+safeZoneH-0.15";
				w = 0.15;
				h = 0.15;
				text = "userconfig\dfs\dfs_stanceindicator\stance_up_run.paa";
			};
		};
	};
};
