#define __imgPath "userconfig\dfs\dfs_stanceindicator"

DFS_fStanceIndicator_animChange =
{
	_anim = toArray (_this select 1);
	if (count _anim > 8 && alive player && cameraOn == player) then
	{
		_pos = [_anim select 4, _anim select 5, _anim select 6, _anim select 7];
		_pos = toLower (toString _pos);
		
		_img = switch (_pos) do
		{
			case "perc":
			{
				"up"
			};
			case "pknl":
			{
				"middle"
			};
			case "ppne":
			{
				"down"
			};
			default
			{
				""
			};
		};
		
		if (_img != "") then
		{
			if (isNil{uiNamespace getVariable "DFS_RscStanceIndicator_idd"} ||
				{isNull (uiNamespace getVariable "DFS_RscStanceIndicator_idd")}) then //Display resource if not already displayed
			{
				46 cutRsc["DFS_RscStanceIndicator", "plain"];
			};
			
			_img = format ["%1\stance_%2_%3.paa", __imgPath, _img, if (isWalking player) then {"walk"} else {"run"}];
			((uiNamespace getVariable "DFS_RscStanceIndicator_idd") displayCtrl 4601) ctrlSetText _img;
		};
	};
};

player addEventHandler ["AnimChanged", {_this call DFS_fStanceIndicator_animChange}];
[player, animationState player] call DFS_fStanceIndicator_animChange;

[] spawn
{
	while {true} do
	{
		_lastWalk = false;
		((uiNamespace getVariable "DFS_RscStanceIndicator_idd") displayCtrl 4601) ctrlSetText "";
		waitUntil {alive player && cameraOn == player};
		while {alive player && cameraOn == player} do
		{
			if (_lastWalk) then
			{
				if (!(isWalking player)) then
				{
					[player, animationState player] call DFS_fStanceIndicator_animChange;
					_lastWalk = isWalking player;
				};
			}
			else
			{
				if (isWalking player) then
				{
					[player, animationState player] call DFS_fStanceIndicator_animChange;
					_lastWalk = isWalking player;
				};
			};
			
			sleep 0.5;
		};
	};
};
